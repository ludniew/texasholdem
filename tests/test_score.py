import pytest
from gamepack.card import Card
from gamepack.player import Player
from gamepack.score import Score


@pytest.fixture()
def score():
    score = Score()
    return score


@pytest.fixture()
def player():
    player = Player("Jim")
    return player


@pytest.mark.parametrize(
    "cards, expected",
    [
        (
            [
                Card(4, 2),
                Card(3, 2),
                Card(2, 3),
                Card(2, 6),
                Card(4, 11),
                Card(3, 14),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(3, 14),
                Card(3, 2),
                Card(2, 3),
                Card(1, 6),
                Card(2, 11),
                Card(1, 14),
                Card(1, 3),
            ],
            True
        ),
(
            [
                Card(3, 11),
                Card(3, 10),
                Card(2, 11),
                Card(1, 10),
                Card(2, 11),
                Card(1, 14),
                Card(1, 3),
            ],
            True
        ),
    ]
)
def test_check_pair(cards, expected, score, player):
    score.cards = cards

    result = score._check_pair()

    assert result == expected


@pytest.mark.parametrize(
    "cards, expected",
    [
        (
            [
                Card(4, 2),
                Card(3, 2),
                Card(2, 3),
                Card(4, 3),
                Card(4, 11),
                Card(3, 14),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(3, 14),
                Card(3, 2),
                Card(2, 3),
                Card(2, 14),
                Card(2, 11),
                Card(1, 14),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(3, 11),
                Card(3, 10),
                Card(2, 11),
                Card(1, 10),
                Card(4, 11),
                Card(1, 14),
                Card(1, 3),
            ],
            True
        ),
    ]
)
def test_check_three(cards, expected, score, player):
    score.cards = cards

    result = score._check_three()

    assert result == expected


@pytest.mark.parametrize(
    "cards, expected",
    [
        (
            [
                Card(4, 11),
                Card(3, 2),
                Card(2, 14),
                Card(4, 3),
                Card(3, 11),
                Card(3, 14),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(3, 7),
                Card(3, 2),
                Card(2, 3),
                Card(2, 14),
                Card(2, 11),
                Card(1, 7),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(3, 11),
                Card(3, 10),
                Card(2, 11),
                Card(1, 10),
                Card(4, 14),
                Card(1, 14),
                Card(1, 3),
            ],
            True
        ),
    ]
)
def test_check_two_pairs(cards, expected, score, player):
    score.cards = cards

    result = score._check_two_pairs()

    assert result == expected


@pytest.mark.parametrize(
    "cards, expected",
    [
        (
            [
                Card(4, 11),
                Card(2, 14),
                Card(2, 11),
                Card(4, 3),
                Card(3, 11),
                Card(3, 14),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(4, 2),
                Card(2, 14),
                Card(2, 2),
                Card(1, 2),
                Card(3, 11),
                Card(3, 3),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(4, 13),
                Card(1, 14),
                Card(2, 7),
                Card(4, 7),
                Card(3, 7),
                Card(2, 14),
                Card(1, 13),
            ],
            True
        ),
    ]
)
def test_check_full(cards, expected, score, player):
    score.cards = cards

    result = score._check_full()

    assert result == expected


@pytest.mark.parametrize(
    "cards, expected",
    [
        (
            [
                Card(4, 11),
                Card(4, 14),
                Card(4, 11),
                Card(4, 3),
                Card(3, 11),
                Card(4, 14),
                Card(1, 3),
            ],
            True
        ),
        (
            [
                Card(4, 2),
                Card(2, 14),
                Card(2, 2),
                Card(1, 2),
                Card(3, 11),
                Card(3, 3),
                Card(1, 3),
            ],
            False
        ),
        (
            [
                Card(1, 13),
                Card(1, 2),
                Card(2, 7),
                Card(4, 7),
                Card(1, 7),
                Card(1, 14),
                Card(1, 10),
            ],
            True
        ),
    ]
)
def test_check_color(cards, expected, score, player):
    score.cards = cards

    result = score._check_color()

    assert result == expected
