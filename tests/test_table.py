from gamepack.player import Player
from gamepack.table import Table


def test_moving_player_with_big_blind_to_the_end():
    original_order = ["1st", "2nd", "3rd", "4rd"]
    expected_order = ["3rd", "4rd", "1st", "2nd"]
    table = Table()
    table.players = [Player(name) for name in original_order]
    table.players[1].is_big_blind = True

    table.move_big_blind_to_the_end_of_players_list()

    assert [player.name for player in table.players] == expected_order
