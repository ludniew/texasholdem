class NewGameException(Exception):
    pass


class EndGameException(Exception):
    pass
