from collections import OrderedDict, Counter


class Score:

    def __init__(self):
        self.cards = None
        self.dict_of_cards_occurrence = None
        self.player = None

    def check_cards(self, cards_on_table, player):
        self.cards = cards_on_table + player.hand
        self.player = player

        return self._check_cards_setup()

    def _check_cards_setup(self):
        score = OrderedDict()

        score["royal_poker"] = self._check_royal_poker()
        score["poker"] = self._check_poker()
        score["carriage"] = self._check_carriage()
        score["full"] = self._check_full()
        score["color"] = self._check_color()
        score["straight"] = self._check_straight()
        score["three"] = self._check_three()
        score["two_pairs"] = self._check_two_pairs()
        score["pair"] = self._check_pair()

        for setup, is_active in score.items():
            if is_active:
                return setup

    def _make_dict_of_all_cards(self):
        dict_cards = {}
        for card in self.cards:
            try:
                dict_cards[card.rank].append(card)

            except KeyError:
                dict_cards[card.rank] = [card]

        return OrderedDict(sorted(dict_cards.items(), reverse=True))

    def _check_pair(self):
        return self._check_same_rank(times=2)

    def _check_two_pairs(self):
        pairs = 0
        for card_rank, list_of_cards in self._make_dict_of_all_cards().items():
            if len(list_of_cards) == 2:
                pairs += 1
            if pairs == 2:
                return True

    def _check_three(self):
        return self._check_same_rank(times=3)

    def _check_straight(self):
        pass

    def _check_color(self):
        suites = [card.suite for card in self.cards]
        counted_suites = Counter(suites)

        return 5 in counted_suites.values()

    def _check_full(self):
        return self._check_pair() and self._check_three()

    def _check_carriage(self):
        return self._check_same_rank(times=4)

    def _check_poker(self):
        pass

    def _check_royal_poker(self):
        pass

    def _check_same_rank(self, times):
        for card_rank, list_of_cards in self._make_dict_of_all_cards().items():
            if len(list_of_cards) == times:
                return True
