import random
from gamepack.card import Card
from gamepack.SETTING import RANKS, SUITS


class Deck:
    def __init__(self):
        self.cards = None
        self.generate_deck()

    def generate_deck(self):
        self.cards = [Card(fig, col) for col in RANKS for fig in SUITS]

    def shuffle(self):
        random.shuffle(self.cards)
        return self

    def pick_a_card(self):
        return self.cards.pop()
