class Pool:
    def __init__(self):
        self.pool = None

    def reinitialize_pool(self, players):
        self.pool = {
            player: 0 for player in players
        }

    def raise_the_pool(self, player, charge):
        self.pool[player] += charge

    def transfer_sumary_to_winner(self, winner):
        pass
