from dataclasses import dataclass


@dataclass
class Card:
    _suite: int
    _rank: int

    def __post_init__(self):
        if self._suite > 4 or self._suite < 1:
            raise AttributeError
        if self._rank > 14 or self._rank < 2:
            raise AttributeError

    @property
    def suite(self):
        return self._suite

    @property
    def rank(self):
        return self._rank

    def show_card(self) -> str:
        return f"{self._translate_rank()} of {self._translate_suite()}"

    def _translate_rank(self) -> str:
        face_of_rank = {11: "J", 12: "Q", 13: "K", 14: "A"}
        return face_of_rank.get(self._rank, self._rank)

    def _translate_suite(self) -> str:
        face_of_suite = {1: "♣", 2: "♢", 3: "♡", 4: "♠"}
        return face_of_suite.get(self._suite, self._suite)

    def __str__(self) -> str:
        return self.show_card()

