from gamepack.deck import Deck
from gamepack.player import Player

import random


class Table:
    def __init__(self):
        self.cards_on_table = None
        self.deck = None
        self.players = []

    def prepare_deck(self):
        self.deck = Deck().shuffle()

    def deal_next_table_card(self):
        self.cards_on_table.append(self.deal_a_card())

    def deal_initial_table_cards(self):
        for _ in range(2):
            self.deal_next_table_card()

    def reinitialize_table(self):
        self.prepare_deck()
        self.cards_on_table = []

        for player in self.players:
            player.hand = []

    def define_players(self):
        number_of_players = int(input("Number of players: "))
        for _ in range(1, number_of_players+1):
            name = str(input(f"Player {_} name: "))
            self.players.append(Player(name))

    def deal_a_card(self):
        return self.deck.pick_a_card()

    def deal_initial_players_cards(self):
        for _ in range(2):
            self.deal_next_players_card()

    def deal_next_players_card(self):
        for player in self.players:
            player.hand.append(self.deal_a_card())

    def pick_player_with_big_blind(self):
        random_player = random.choice(self.players)
        random_player.is_big_blind = True

    def move_big_blind_to_the_end_of_players_list(self):
        while 1:
            self.move_first_player_to_the_end()
            if self.players[-1].is_big_blind:
                break

    def move_first_player_to_the_end(self):
        self.players.append(self.players.pop(self.players.index(self.players[0])))

    def reset_players_in_game_status(self):
        for player in self.players:
            player.is_in_game = True

    def move_raising_player_to_the_begining(self):
        while 1:
            self.move_first_player_to_the_end()
            if self.players[0].is_raising_pool:
                break

    def move_raising_player_to_the_end(self):
        while 1:
            self.move_first_player_to_the_end()
            if self.players[-1].is_raising_pool:
                break

    def is_any_of_players_raises_the_pool(self):
        return any(
            (player.is_raising_pool for player in self.players)
        )

    def reset_players_raise_status(self):
        for player in self.players:
            player.is_raising_pool = False

    def players_in_game(self):
        return [player for player in self.players if player.is_in_game]

    def get_raising_player(self):
        for player in self.players:
            if player.is_raising_pool:
                return player

    def choose_blind_picks(self):
        self.pick_player_with_big_blind()
        self.move_big_blind_to_the_end_of_players_list()
        self.players[-2].is_small_blind = True

    def change_blind_picks(self):
        self.move_big_blind_to_the_end_of_players_list()
        self.players[-1].is_big_blind = False
        self.players[-2].is_small_blind = False
        self.move_first_player_to_the_end()
        self.players[-1].is_big_blind = True
        self.players[-2].is_small_blind = True
