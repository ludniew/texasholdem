from gamepack.table import Table
from gamepack.pool import Pool
from gamepack.score import Score
from exceptions.exceptions import (
    EndGameException,
    NewGameException,
)
from gamepack.SETTING import BIG, SMALL


class Game:
    def __init__(self):
        self.table = Table()
        self.pool = Pool()
        self.score = Score()

    def game_menu(self):
        print(
            "<1> Play\n"
            "<2> Exit\n"
            "Choice: "
        )
        choice = int(input())
        if choice == 1:
            if not self.table.players:
                self.table.define_players()
                self.play(new_game=True)

            else:
                self.play(new_game=False)
        elif choice == 2:
            self.end_game()

    def play(self, new_game):
        if new_game:
            self.pool.reinitialize_pool(self.table.players)
            self.table.choose_blind_picks()

        else:
            self.table.change_blind_picks()

        self.initiate_pool_with_small_and_big_blind()
        self.reinitialize_game()

        for frame in range(5):
            print(f"Round: {frame + 1}")
            for index, player in enumerate(self.table.players, start=1):
                print(f"Player{index}: {player.cash}")

            print(f"Pool: {self.pool.pool}")
            if frame == 0:
                self.table.deal_initial_players_cards()

            while 1:
                every_player_in = self.negotiations_process()
                if every_player_in:
                    break

            if frame == 0:
                self.table.deal_initial_table_cards()

            else:
                self.table.deal_next_table_card()

        self.define_winner()

        self.table.reset_players_in_game_status()

    def negotiations_process(self):
        for player in self.table.players_in_game():
            if player.is_raising_pool:
                continue
            if self.table.is_any_of_players_raises_the_pool():
                print(
                    f"Player '{player.name}' move\n"
                    "<1> In\n"
                    "<2> Raise\n"
                    "<3> Fold\n"
                )
            else:
                print(
                    f"Player '{player.name}' move\n"
                    "<1> Call\n"
                    "<2> Raise\n"
                    "<3> Fold\n"
                )
            choice = int(input("Choice: "))
            if choice == 1:
                if self.table.is_any_of_players_raises_the_pool():
                    charge = self.pool.pool[self.table.get_raising_player()] - self.pool.pool[player]
                    self.charge_player_and_add_to_pool(player, charge)

                continue

            elif choice == 2:
                self.table.reset_players_raise_status()
                player.is_raising_pool = True

                raise_charge = int(input("Raise for: "))
                self.charge_player_and_add_to_pool(player, raise_charge)

                self.table.move_raising_player_to_the_begining()
                return self.negotiations_process()

            elif choice == 3:
                player.is_in_game = False
                if len(self.table.players_in_game()) == 1:
                    self.new_game()

        else:
            self.table.reset_players_raise_status()
            return True

    def initiate_pool_with_small_and_big_blind(self):
        for player in self.table.players:
            if player.is_small_blind:
                self.charge_player_and_add_to_pool(player, SMALL)

            elif player.is_big_blind:
                self.charge_player_and_add_to_pool(player, BIG)

    def charge_player_and_add_to_pool(self, player, charge):
        player.cash -= charge
        self.pool.raise_the_pool(player, charge)

    def reinitialize_game(self):
        self.table.reinitialize_table()
        self.table.reset_players_in_game_status()

    def define_winner(self):
        pass

    def end_game(self):
        raise EndGameException

    def new_game(self):
        raise NewGameException
