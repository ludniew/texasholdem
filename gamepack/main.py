from gamepack.game import Game
from gamepack.SETTING import RUN
from exceptions.exceptions import (
    EndGameException,
    NewGameException,
)


game = Game()

while RUN:
    print("-------------\nNEW GAME\n-------------")
    try:
        game.game_menu()

    except NewGameException:
        continue

    except EndGameException:
        break
