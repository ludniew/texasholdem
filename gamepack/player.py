class Player:
    def __init__(self, name):
        self.name = name
        self.hand = []
        self.cash = 1000
        self.is_big_blind = False
        self.is_small_blind = False
        self.is_in = False
        self.is_raising_pool = False
        self.is_in_game = True

    def show_hand(self):
        print([card.show_card() for card in self.hand])

    def __str__(self):
        return self.name
